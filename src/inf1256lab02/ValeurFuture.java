/*
 * Author: Johnny Tsheke @UQAM
 * cette solution ne tient pas compte des exceptions
 * Calcul de la valeur future
 */
package inf1256lab02;
import java.util.*;// pour manipuler clavier avec Scanner
public class ValeurFuture {
   
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	  final double CENT = 100.0;
	  Scanner sc = new Scanner(System.in);//pour lire au clavier
	  System.out.println("Entrez le capital svp");
	  double capital =sc.nextDouble();
	  System.out.println("Entrez le pourcentage du taux d'intérêt svp");
	  double taux = sc.nextDouble();
	  double interet = capital * (taux/CENT);
	  double valeurFuture = capital +interet;
	  System.out.println("Capital = "+capital);
	  System.out.println("Taux d'intérêt en % = "+taux);
	  System.out.println("Intérêt sur une période = "+interet);
	  System.out.println("Valeur future = "+valeurFuture);
	}

}
